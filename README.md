# README #
### Purpose ###
Built as part of SPAR-99 (https://umbria.atlassian.net/browse/SPAR-99).
This is an investigation into having a centralised configuration service so that our apps don't require new releases just to get new config.

This is based on the Spring guide: https://spring.io/guides/gs/centralized-configuration/

### Usage ###
To make use of this, you will need the configuration service and client found at:
https://bitbucket.org/colinheyl/config-service/src/master/